<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index')->name('home');

//News
Route::get('/create-news', 'NewsController@create')->name('news');
Route::post('/store-news', 'NewsController@store')->name('news.create');
Route::get('/show-news/{id}', 'NewsController@show')->name('news.show');

//Category
Route::get('/create-category', 'CategoryController@create')->name('category');
Route::post('/store-category', 'CategoryController@store')->name('category.create');