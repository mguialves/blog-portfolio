<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', 'BlogController@index')->name('home');

//News
Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/create-news', 'NewsController@create')->name('news');
Route::post('/store-news', 'NewsController@store')->name('news.create');
Route::get('/show-news/{id}', 'NewsController@show')->name('news.show');

//Category
Route::get('/create-category', 'CategoryController@create')->name('category');
Route::post('/store-category', 'CategoryController@store')->name('category.create');