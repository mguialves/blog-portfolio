@extends('dashboard.layout.app')

@section('content')
    @if (session('message'))
        <div class="alert alert-success" role="alert">
            {{session('message')}}
        </div>  
    @endif
    @forelse ($news as $new)
        <div class="col-3">
            <div class="row">
                <div class="card" id="card" style="width: 18rem;">
                    <img class="card-img-top img-thumbnail rounded mx-auto d-block" src="{{ url("storage/{$new->img}") }}" alt="Image da Noticia {{ $new->title }}">
                    <div class="card-body">
                        <h5 class="card-title">{{ $new->title }}</h5>
                        <p class="card-text">{!! Str::limit($new->descript, '20') !!}</p>
                        <a href="{{ route('news.show', $new->id) }}">Ver mais</a>
                        <hr>
                        <p style="font-size: 13px;">Categoria: {{ $new->categories['title'] }}</p>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <div class="col-12" id="noNews">
            <div class="row">
                <p>Nenhuma noticia para ser exibida...</p>
            </div>
        </div>
    @endforelse
    
@endsection