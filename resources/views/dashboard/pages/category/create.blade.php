@extends('dashboard.layout.app')

@section('content')
    @if ($errors->any())    
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                {{$error}} <br />
            @endforeach
        </div>
    @endif
    <div class="col-4" id="category">
        <div class="row">
            <h1>Criar Categoria</h1>
            <hr>
            <form action="{{ route('category.create') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div id="btn-right">
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
                    <div class="form-group">
                        <label>Título</label>
                        <input type="text" class="form-control" name="title" placeholder="Digite o título">
                    </div>
                <br />
                <a href="{{ url()->previous() }}" class="btn btn-default">Voltar</a>
            </form>
        </div>
        <br />
        <br />
        <div class="row">
            <h1>Categorias Criadas</h1>
            <hr>
            <ul>
                @foreach ($categories as $category)
                    <li>{{ $category->title }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection