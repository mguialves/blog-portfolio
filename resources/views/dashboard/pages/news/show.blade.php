@extends('dashboard.layout.app')

@section('content')
    <div class="row">
        <div class="col-9" id="card-show">
            <div class="card">
                <h5 class="card-header">{{ $news->title }}</h5>
                <div class="card-body">
                    <img src="{{ url("storage/{$news->img}") }}" class="img-responsive rounded mx-auto d-block" alt="{{ $news->title }}">
                    <hr>
                    <p class="card-text">{!! $news->descript !!}</p>
                </div>
                <div class="card-footer">
                    <h6>Categoria: {{ $news->categories['title'] }}</h6>
                </div>
            </div>
        </div>
    </div>
@endsection