@extends('dashboard.layout.app')

@section('content')
    @if ($errors->any())    
        <div class="alert alert-danger" role="alert">
            @foreach ($errors->all() as $error)
                {{$error}} <br />
            @endforeach
        </div>
    @endif
    <div class="col-6" id="news">
        <div class="row">
            <h1>Criar Notícia</h1>
            <form action="{{ route('news.create') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div id="btn-right">
                    <button type="submit" class="btn btn-primary">Postar</button>
                </div>
                    <div class="form-group">
                        <label>Título</label>
                        <input type="text" class="form-control" name="title" placeholder="Digite o título">
                    </div>
                    <div class="form-group">
                        <label>Categoria</label>
                        <select class="form-control" name="category_id">
                                <option selected disabled>Selecione</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Descrição</label>
                        <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
                        <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
                        <textarea class="form-control" name="descript" rows="6"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Imagem</label>
                        <input type="file" name="img" class="form-control">
                    </div>
                <br />
                <a href="{{ url()->previous() }}" class="btn btn-default">Voltar</a>
            </form>
        </div>
    </div>
@endsection