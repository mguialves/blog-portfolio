<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{post, Category};
use Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = post::with('categories')
            ->get();

        return response()->json(
            $news
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $news = post::all();

        return response()->json(
            $news,
            $categories
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validatorNews($request);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $news = $request->only('title', 'descript', 'category_id');

        $blog_img = $request->file('img')->store('img-post');

        post::create([
            'title' => $news['title'],
            'descript' => $news['descript'],
            'category_id' => $news['category_id'],
            'img' => $blog_img,
        ]);

        return redirect()->route('news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = post::with('categories')->find($id);

        return response()->json(
            $news
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function validatorNews($request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'descript' => 'required',
            'category_id' => 'required',
            'img' => 'required',
        ]);

        $validator->setAttributeNames([
            'title' => 'Titulo',
            'descript' => 'Descrição',
            'category_id' => 'Categoria',
            'img' => 'Imagem',
        ]);

        return $validator;
    }
}
