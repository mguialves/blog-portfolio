<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable = [
        'title', 'category_id', 'descript', 'img',
    ];

    public function categories()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
